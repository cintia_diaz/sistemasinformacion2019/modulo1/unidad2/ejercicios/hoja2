﻿USE ciclistas;

/*
  H2 - Consultas de selección 2
  */

-- 1.1 Número de ciclistas que hay
SELECT COUNT(*) FROM ciclista c;

-- 1.2 Número de ciclistas que hay del equipo Banesto
SELECT * FROM ciclista c WHERE c.nomequipo = 'Banesto';

SELECT COUNT(*) FROM (SELECT * FROM ciclista c WHERE c.nomequipo = 'Banesto') c1;

-- 1.3 La edad media de los ciclistas
  SELECT AVG(c.edad) FROM ciclista c;
  SELECT AVG(DISTINCT c.edad) FROM ciclista c;

-- 1.4 La edad media de los de equipo Banesto
  SELECT AVG(DISTINCT c.edad) FROM ciclista c WHERE c.nomequipo='Banesto';


-- 1.5	La edad media de los ciclistas por cada equipo
  SELECT c.nomequipo,AVG(c.edad) FROM ciclista c GROUP BY c.nomequipo;

-- 1.6	El número de ciclistas por equipo
  SELECT nomequipo, COUNT(*) FROM ciclista c GROUP BY c.nomequipo;

-- 1.7	El número total de puertos
  SELECT COUNT(*) FROM puerto p;

-- 1.8	El número total de puertos mayores de 1500
  SELECT COUNT(*) FROM puerto p WHERE p.altura>1500;

-- 1.9	Lista el nombre de los equipos que tengan más de 4 ciclistas
  SELECT COUNT(*) FROM ciclista c GROUP BY c.nomequipo;

  SELECT c1.nomequipo FROM (SELECT c.nomequipo, COUNT(*) cuenta FROM ciclista c GROUP BY c.nomequipo) c1 WHERE c1.cuenta>4;

  SELECT c.nomequipo FROM ciclista c GROUP BY c.nomequipo HAVING COUNT(*)>4;

-- 1.10	Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32
  SELECT c.nomequipo,COUNT(*) FROM ciclista c GROUP BY c.nomequipo;

  SELECT c1.nomequipo FROM (SELECT c.nomequipo, c.edad, COUNT(*) cuenta FROM ciclista c GROUP BY c.nomequipo, c.edad) c1 WHERE c1.edad BETWEEN 28 AND 32 AND c1.cuenta >4;

  SELECT c1.nomequipo 
    FROM 
      (SELECT c.nomequipo, c.edad FROM ciclista c GROUP BY c.nomequipo, c.edad) c1 
    WHERE c1.edad 
    BETWEEN 28 AND 32 
    HAVING COUNT(*)>4;

-- 1.11 Indícame el número de etapas que ha ganado cada uno de los ciclistas
  SELECT dorsal, COUNT(*) FROM etapa e GROUP BY e.dorsal;

-- 1.12 Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa
  SELECT dorsal, COUNT(*) FROM etapa e GROUP BY e.dorsal;

  SELECT e.dorsal FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1;
